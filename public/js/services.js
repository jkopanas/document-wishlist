var DocumentEtravelServices = angular.module('DocumentEtravelServices', [
	'LocalStorageModule',
	'restangular'
]);

DocumentEtravelServices.factory('userService', ['$http', 'localStorageService','Restangular', function($http, localStorageService, Restangular) {

	function checkIfLoggedIn() {

		if(localStorageService.get('token'))
			return true;
		else
			return false;

	}

	function signup(name, email, password, onSuccess, onError) {

		$http.post('/api/auth/signup', 
		{
			name: name,
			email: email,
			password: password
		}).
		then(function(response) {

			onSuccess(response);

		}, function(response) {

			onError(response);

		});

	}

	function login(email, password, onSuccess, onError){

		$http.post('/api/auth/login', 
		{
			email: email,
			password: password
		}).
		then(function(response) {

			localStorageService.set('token', response.data.token);
            Restangular.setDefaultRequestParams({ "token": response.data.token || '' });
			onSuccess(response);

		}, function(response) {

			onError(response);

		});

	}

	function logout(){

		localStorageService.remove('token');

	}

	function getCurrentToken(){
		return localStorageService.get('token');
	}

	return {
		checkIfLoggedIn: checkIfLoggedIn,
		signup: signup,
		login: login,
		logout: logout,
		getCurrentToken: getCurrentToken
	}

}]);

DocumentEtravelServices.factory('documentService', ['Restangular', 'userService', function(Restangular, userService) {

	function getAll(start, number, tableState,onSuccess, onError){
		Restangular.all('api/documents').getList({'start': start, 'perPage': number}).then(function(response){
			onSuccess(response);
		}, function(response){
			onError(response);
		});
	}

	function getById(documentId, onSuccess, onError){

		Restangular.one('api/documents', documentId).get().then(function(response){

			onSuccess(response);

		}, function(response){

			onError(response);

		});

	}

	function create(data, onSuccess, onError){

		Restangular.all('api/documents').post(data).then(function(response){
			onSuccess(response);
		
		}, function(response){
			onError(response);
		
		});

	}


	function remove(documentId, onSuccess, onError){
		Restangular.one('api/documents/', documentId).remove().then(function(){

			onSuccess();

		}, function(response){

			onError(response);

		});
	}

	return {
		getAll: getAll,
		getById: getById,
		create: create,
		remove: remove
	}

}]);