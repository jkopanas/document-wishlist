var DocumentEtravelControllers = angular.module('DocumentEtravelControllers', [
	'DocumentEtravelServices'
]);

DocumentEtravelControllers.controller('LoginController', ['$scope', '$location', 'userService', 'localStorageService', function ($scope, $location, userService, localStorageService) {

	$scope.login = function() {

        if ($scope.userForm.$valid) {
            userService.login(
                $scope.email, $scope.password,
                function (response) {
                    $location.path('/');
                },
                function (response) {
                    $scope.notification = {
                        status: 'show',
                        message: 'Wrong Credentials! Password and Email are not correct',
                        type: "danger"
                    };
                }
            );
        }
	}

	$scope.email = '';
	$scope.password = '';

	if(userService.checkIfLoggedIn()) {
        $location.path('/');
    }

}]);

DocumentEtravelControllers.controller('SignupController', ['$scope', '$location', 'userService', function ($scope, $location, userService) {

	$scope.signup = function() {

        if ($scope.userForm.$valid) {
            userService.signup(
                $scope.name, $scope.email, $scope.password,
                function (response) {
                    $location.path('/login');
                },
                function (response) {
                    $scope.notification = {
                        status: 'show',
                        message: 'Error! '+response.data.error.message,
                        type: "danger"
                    };
                }
            );
        }
	}

	$scope.name = '';
	$scope.email = '';
	$scope.password = '';
    $scope.repassword = '';

	if(userService.checkIfLoggedIn()) {
        $location.path('/');
    }

}]);

DocumentEtravelControllers.controller('MainController', ['$scope', '$location', 'userService', 'documentService', function ($scope, $location, userService, documentService) {

    if(userService.checkIfLoggedIn()) {
        $location.path('/');
    } else {
        $location.path('/login');
    }

	$scope.logout = function(){
		userService.logout();
		$location.path('/login');
	}
    $scope.token = userService.getCurrentToken();

	$scope.remove = function(documentId){

		if(confirm('Are you sure to remove this document from your wishlist?')){
			documentService.remove(documentId, function(){

                $scope.notification = {
                    status: 'show',
                    message: 'The Document removed successfully',
                    type: "success"
                };

                for (var i=0; i<$scope.displayed.length ;i++) {
                    if ($scope.displayed[i].id == documentId ) {
                        $scope.displayed.splice(i, 1);
                        $scope.documents = parseInt($scope.documents) - 1;
                    }
                }

			}, function(){

				alert('Some errors occurred while communicating with the service. Try again later.');

			});
		}

	}

    $scope.displayed = [];

    $scope.callServer = function callServer(tableState) {

        if(!userService.checkIfLoggedIn()) {
            $location.path('/login');
        }
        $scope.isLoading = true;

        var pagination = tableState.pagination;

        var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
        var number = pagination.number || 5;  // Number of entries showed per page.

        documentService.getAll(start, number, tableState,function(data){
            $scope.displayed = data;
            $scope.documents = data.count;
            tableState.pagination.numberOfPages = (parseInt(data.count) % number) == 0? (parseInt(data.count)/number) : Math.floor((parseInt(data.count)/number) + 1);
            $scope.isLoading = false;
        },function(){});
    }

}]);


DocumentEtravelControllers.controller('UploadController', ['$scope', '$location', 'userService', 'documentService', function ($scope, $location, userService, documentService) {

    $scope.download = false;
    $scope.create = function(){

        if ($scope.userForm.$valid) {
            documentService.create({
                name: $scope.name,
                document: $scope.document
            }, function (e) {
                $scope.notification = {
                    status: 'show',
                    message: 'Success! Your document is uploaded.',
                    type: "success"
                };

                $scope.name = "";
                $scope.download = true;
                $scope.document.url = e;

            }, function () {
                $scope.notification = {
                    status: 'show',
                    message: 'Error! An error occured, your document is not uploaded.',
                    type: "danger"
                };

                $scope.name = "";
            });
        }

    };

    if(!userService.checkIfLoggedIn()) {
        $location.path('/login');
    }

}]);
