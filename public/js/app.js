var DocumentEtravel = angular.module('DocumentEtravel', [
  'ngRoute',
  'DocumentEtravelControllers',
  'ng-file-model',
  'notification',
  'loading',
  'smart-table'
]);

DocumentEtravel.config(function (localStorageServiceProvider) {
  localStorageServiceProvider
    .setPrefix('DocumentEtravel')
    .setStorageType('localStorage');
});

DocumentEtravel.run(["localStorageService",'Restangular',function(localStorageService,Restangular) {
    Restangular.setDefaultRequestParams({ "token": localStorageService.get('token') || '' });

    // add a response interceptor
    Restangular.addResponseInterceptor(function(data, operation, what, url, response, deferred) {
        var extractedData;
        // .. to look for getList operations
        if (operation === "getList") {
            // .. and handle the data and meta data
            extractedData = data.results;
            extractedData.count = data.count;
        } else {
            extractedData = data;
        }
        return extractedData;
    });

    Restangular.setErrorInterceptor(
        function ( response ) {
            if ( response.status == 401 ) {

                localStorageService.remove('token');
                window.location = "#/login";
            }
            return false;
        }
    );
}]);


DocumentEtravel.config(['$routeProvider', 'RestangularProvider', function($routeProvider, RestangularProvider) {


	$routeProvider.
	when('/login', {
	    templateUrl: 'partials/login.html',
	    controller: 'LoginController'
	}).
	when('/signup', {
	    templateUrl: 'partials/signup.html',
	    controller: 'SignupController'
	}).
	when('/upload', {
		templateUrl: 'partials/upload.html',
		controller: 'UploadController'
	}).
	when('/', {
	    templateUrl: 'partials/index.html',
	    controller: 'MainController'
	}).
	otherwise({
	    redirectTo: '/login'
	});

}]);