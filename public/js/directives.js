/**
 * Created by Giannis on 28/1/2017.
 */
(function () {
    'use strict';
    angular.module('ng-file-model', [])
        .directive("ngFileModel", [function () {
            return {
                scope: {
                    ngFileModel: "="
                },
                link: function (scope, element, attributes) {
                    element.bind("change", function (changeEvent) {
                        var reader = new FileReader();
                        reader.onload = function (loadEvent) {
                            scope.$apply(function () {
                                scope.ngFileModel = {
                                    lastModified: changeEvent.target.files[0].lastModified,
                                    lastModifiedDate: changeEvent.target.files[0].lastModifiedDate,
                                    name: changeEvent.target.files[0].name,
                                    size: changeEvent.target.files[0].size,
                                    type: changeEvent.target.files[0].type,
                                    data: loadEvent.target.result.split(',')[1]
                                };
                            });
                        }
                        reader.readAsDataURL(changeEvent.target.files[0]);
                    });
                }
            }
        }]);

    angular.module('notification', [])
        .directive('notification', ['$timeout', function ($timeout) {
            return {
                restrict: 'A',
                controller: ['$scope', function ($scope) {
                    $scope.notification = {
                        status: 'hide',
                        type: 'success',
                        message: ''
                    };
                }],
                link: function(scope, elem, attrs) {
                    // watch for changes
                    attrs.$observe('notification', function (value) {
                        if (value === 'show') {
                            // shows alert
                            $(elem).show();

                            // and after 3secs
                            $timeout(function () {
                                // hide it
                                $(elem).hide();

                                // and update the show property
                                scope.notification.status = 'hide';
                            }, 3000);
                        }
                    });
                }
            };
        }]);

    angular.module('loading',[])
        .directive('loading', ['$http', function ($http) {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    scope.isLoading = function () {
                        return $http.pendingRequests.length > 0;
                    };
                    scope.$watch(scope.isLoading, function (value) {
                        if (value) {
                            element.removeClass('ng-hide');
                        } else {
                            element.addClass('ng-hide');
                        }
                    });
                }
            };
        }]);

})();