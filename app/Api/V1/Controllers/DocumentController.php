<?php
namespace App\Api\V1\Controllers;
use JWTAuth;
use Validator;
use App\Document;
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input as Input;
use App\Sns\SnsBroadcaster as SnsBroadcaster;
use Dingo\Api\Routing\Helpers;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Mockery\CountValidator\Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DocumentController extends Controller
{
    use Helpers;
    public function index()
    {
        $count = $this->currentUser()
            ->documents()
            ->get()
            ->count();

        $results = $this->currentUser()
            ->documents()
            ->orderBy('created_at', 'DESC')
            ->skip(Input::get('start'))
            ->take(Input::get('perPage'))
            ->get()
            ->toArray();

        Log::info(" User:".$this->currentUser()->email." reads documents");
        return ['count' => $count,'results' => $results];

    }


    public function show($id)
    {
        try {
            $document = Document::where([['legacy_document_id','=',$id], ['user_id','=',$this->currentUser()->id]])->first();
        } catch (Exception $e) {
            Log::error(" User:".$this->currentUser()->email." tried to read a document that does not exist");
            throw new NotFoundHttpException;
        }

        $app= App::getFacadeRoot();
        $legacy_api = $app->make('LegacyApi');
        $content = $legacy_api->download($id);

        $finfo = new \finfo(FILEINFO_MIME);
        $mime_info = explode(";",$finfo->buffer($content));


        header('Content-Type: '.$mime_info[0]);
        header('Content-Transfer-Encoding: '.explode("=",$mime_info[1])[1]);
        header('Content-disposition: attachment; filename="'.$document->filename.'"');
        echo $content;
        exit;
    }


    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'document' => 'required'
        ]);

        if ($validator->fails()) {
            Log::error(" User:".$this->currentUser()->email." Failed uploading file");
            return $this->response->errorBadRequest();
        }


        $document = new Document;
        $document->name = $request->get('name');
        $document->user_id = $this->currentUser()->id;
        $document->type = $request->get('document')['type'];
        $document->filename = $request->get('document')['name'];
        $document->content = $request->get('document')['data'];

        try {
            if ($this->currentUser()->documents()->save($document)) {
                Log::info(" User:".$this->currentUser()->email." uploaded and saved document successfully here: ".$document->document_url);
                return $this->response->array($document->document_url)->setStatusCode(201);
            } else {
                Log::info(" User:".$this->currentUser()->email." could not upload the document");
                return $this->response->error('could_not_create_document', 500);
            }
        } catch (Exception $e) {
            Log::info(" User:".$this->currentUser()->email." could not upload the document");
            $this->response->error('could_not_upload_document', 500);
        }

    }

    public function destroy($id)
    {
        $document = $this->currentUser()->documents()->find($id);
        if(!$document)
            throw new NotFoundHttpException;
        if($document->delete())
            return $this->response->noContent();
        else
            return $this->response->error('could_not_delete_document', 500);
    }

    private function currentUser() {
        return JWTAuth::parseToken()->authenticate();
    }
}