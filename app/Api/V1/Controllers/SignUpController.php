<?php

namespace App\Api\V1\Controllers;

use Config;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use Dingo\Api\Routing\Helpers;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SignUpController extends Controller
{
    use Helpers;
    public function signUp(LoginRequest $request, JWTAuth $JWTAuth)
    {
        if (User::where('email','=', $request->email)->first()) {
            Log::info(" User: tried to signup with an already registered account");
            return $this->response->error('This email already exists', 400);
        }
        $user = new User($request->all());
        if(!$user->save()) {
            Log::error(" User: ".$request->email." failed to Signup");
            throw new HttpException(500);
        }

        if(!Config::get('etravel.sign_up.release_token')) {
            return response()->json([
                'status' => 'ok'
            ], 201);
        }

        $token = $JWTAuth->fromUser($user);
        Log::info(" User: ".$request->email." with token: ".$token." signup successfully!");
        return response()->json([
            'status' => 'ok',
            'token' => $token
        ], 201);
    }
}
