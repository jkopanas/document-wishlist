<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class LoginController extends Controller
{
    public function login(LoginRequest $request, JWTAuth $JWTAuth)
    {

        $credentials = $request->only(['email', 'password']);

        try {
            $token = $JWTAuth->attempt($credentials);

            if(!$token) {
                Log::error(" Access denied  Wrong Credentials");
                throw new AccessDeniedHttpException();
            }

        } catch (JWTException $e) {
            Log::error(" User: ".$credentials['email']." failed login");
            throw new HttpException(500);
        }

        Log::info(" User: ".$credentials['email']." with token: ".$token." login successfully!");
        return response()
            ->json([
                'status' => 'ok',
                'token' => $token
            ]);
    }
}
