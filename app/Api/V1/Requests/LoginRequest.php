<?php

namespace App\Api\V1\Requests;

use Config;
use Dingo\Api\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Log;

class LoginRequest extends FormRequest
{
    public function rules()
    {
        return Config::get('etravel.login.validation_rules');
    }

    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        if ($this->container['request'] instanceof Request) {
            throw new ValidationHttpException($validator->errors());
        }

        Log::info("User's Request failed at Validating data");
        parent::failedValidation($validator);
    }


}
