<?php

namespace App\Providers;

use Aws\Sns\SnsClient as SnsClient;
use Aws\Credentials\Credentials;
use App\Sns\SnsBroadcaster;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;


class AWSsnsServiceProvider extends ServiceProvider
{

    protected $defer = false;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('SnsBroadcaster', function ($app) {
            $credentials = new Credentials($app['config']['etravel.aws_key'],$app['config']['etravel.aws_secret']);
            return new SnsBroadcaster(new SnsClient(array(
                'credentials' => $credentials,
                'version' => 'latest',
                'region' => $app['config']['etravel.aws_region']
            )));
        });
    }



    public function provides()
    {
        return array('SnsBroadcaster');
    }
}
