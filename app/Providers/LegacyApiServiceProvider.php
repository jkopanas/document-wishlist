<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\LegacyApi\LegacyApi as LegacyApi;

class LegacyApiServiceProvider extends ServiceProvider
{


    protected $defer = false;
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('LegacyApi', LegacyApi::class);
    }



    public function provides()
    {
        return ['LegacyApi'];
    }
}
