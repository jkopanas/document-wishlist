<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\LegacyApi\Curl\CurlService as CurlService;

class CurlServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('Curl', function () {
            return new CurlService();
        }
        );
    }


    /**
     * @return array
     */
    public function provides()
    {
        return array('Curl');
    }
}
