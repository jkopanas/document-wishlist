<?php
/**
 * Created by PhpStorm.
 * User: Giannis
 * Date: 29/1/2017
 * Time: 12:05 μμ
 */
namespace App\Sns;

use Aws\Sns\SnsClient;
use Illuminate\Support\Facades\Log;

class SnsBroadcaster
{
    protected $client;
    public function __construct(SnsClient $client)
    {
        $this->client = $client;
    }

    public function broadcast(array $channels, $event, array $payload = array())
    {
        foreach ($channels as $channel) {
            $response = $this->client->publish(array(
                'TopicArn' => $channel,
                'Message' => json_encode($payload),
                'Subject' => $event
            ));
            Log::info(" User:".$payload['user_email']."  broadcast to aws an event: ".$event);
        }
    }
}