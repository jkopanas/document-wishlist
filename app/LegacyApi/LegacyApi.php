<?php
/**
 * Created by PhpStorm.
 * User: Giannis
 * Date: 28/1/2017
 * Time: 2:21 μμ
 */

namespace App\LegacyApi;

use App\Document as Document;
use App\LegacyApi\Curl\Curl;
use App\LegacyApi\Curl\CurlService as CurlService;
use Mockery\CountValidator\Exception;

class LegacyApi
{
    private $document = '';


    public function setDocument(Document $document)
    {
        $this->document = $document;
    }


    public function upload ()
    {

        $data = json_encode(array(
            'content_type' => $this->document->type,
            'content' => $this->document->content
        ));

        $response = Curl::to(config('etravel.legacy_url') . "/upload")
            ->withHeader('Content-Type: application/json')
            ->withData($data)
            ->returnResponseObject()
            ->post();
        
        return $response->content;

    }

    public function download ($id)
    {
        $response = Curl::to(config('etravel.legacy_url')."/retrieve/".$id)
            ->returnResponseObject()
            ->get();

        return $response->content;
    }
}