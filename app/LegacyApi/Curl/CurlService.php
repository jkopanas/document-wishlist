<?php
/**
 * Created by PhpStorm.
 * User: Giannis
 * Date: 28/1/2017
 * Time: 3:08 μμ
 */

namespace App\LegacyApi\Curl;


class CurlService {
    /**
     * @param $url string   The URL to which the request is to be sent
     * @return \App\Legacy\Curl\Builder
     */
    public function to($url)
    {
        $builder = new Builder();
        return $builder->to($url);
    }
}