<?php
/**
 * Created by PhpStorm.
 * User: Giannis
 * Date: 28/1/2017
 * Time: 3:19 μμ
 */

namespace App\LegacyApi\Curl;

use Illuminate\Support\Facades\Facade;

class Curl extends Facade {
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Curl';
    }
}