<?php
/**
 * Created by PhpStorm.
 * User: Giannis
 * Date: 28/1/2017
 * Time: 10:49 πμ
 */

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Mockery\CountValidator\Exception;

class Document extends Model
{
    protected $table = "user_documents";
    protected $fillable = ['name'];


    public static function boot()
    {
        parent::boot();

        self::saving(function ($model) {


            $legacy_api = resolve("LegacyApi");
            $legacy_api->setDocument($model);

            $response  = json_decode($legacy_api->upload());

            if ($response) {
                $model->legacy_document_id = $response->Id;
                $model->document_url = $response->Url;
                unset($model->content);
                unset($model->type);
                Log::info(" User:".User::find($model->user_id)->first()->email." uploaded document to Legacy Api legacy_id: ".$response->Id);
            } else {
                Log::info(" User:".User::find($model->user_id)->first()->email." could not upload the document");
                throw new Exception();
            }


        });

        self::saved(function($model) {

            $payload = [
              'user_email' => User::find($model->user_id)->email,
              'uploaded_at' => $model->created_at->toIso8601String(),
              'uploaded_document_id' => $model->legacy_document_id,
              'uploaded_document_url' => $model->document_url,
            ];

            $sns = resolve('SnsBroadcaster');
            $sns->broadcast([config('etravel.arn_topic')],"Document uploaded", $payload);

        });
    }




}