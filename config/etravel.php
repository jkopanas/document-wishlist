<?php

return [


    'legacy_url' => 'https://qzx3nzxfeg.execute-api.eu-west-1.amazonaws.com/casestudy',

    'aws_key' => 'AKIAJ75BUGWYBPEAFFVA',
    'aws_secret'  => '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',
    'aws_region' => 'eu-west-1',
    'arn_topic' => 'arn:aws:sns:eu-west-1:473424689532:ETravel_FullStackDeveloperCaseStudy',

    'sign_up' => [
        'release_token' => env('SIGN_UP_RELEASE_TOKEN'),
        'validation_rules' => [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]
    ],

    'login' => [
        'validation_rules' => [
            'email' => 'required|email',
            'password' => 'required'
        ]
    ],

    'forgot_password' => [
        'validation_rules' => [
            'email' => 'required|email'
        ]
    ],

    'reset_password' => [
        'release_token' => env('PASSWORD_RESET_RELEASE_TOKEN', false),
        'validation_rules' => [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed'
        ]
    ]

];
