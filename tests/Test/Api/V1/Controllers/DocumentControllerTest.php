<?php

namespace App\Test\Api\V1\Controllers;

use JWTAuth;
use Auth;
use Hash;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class DocumentControllerTest extends TestCase
{

    use DatabaseMigrations;

    public $token = '';
    public $file = '';
    public $wrongfile = '';
    public $user = '';

    public function setUp()
    {
        parent::setUp();

        $this->user = new User([
            'name' => 'Test',
            'email' => 'test@email.com',
            'password' => '123456'
        ]);

        $this->user->save();


        $this->token = JWTAuth::fromUser($this->user);

        JWTAuth::setToken($this->token);

        Auth::login($this->user);

        $this->file = storage_path() . '/app/public/test.txt';
        $this->wrongfile = storage_path() . '/app/public/wrongtest.txt';

    }

    public function testUploadDocumentSuccessfully()
    {
        //var_dump($this->upload("test")->response->content());
        $this->upload("test")->assertResponseStatus(201);

    }

    public function testWrongUploadMimeTypeDocument()
    {
        $datetime = new \DateTime();


        $this->json('POST', 'api/documents?token='.$this->token, [
            'name' => 'test',
            'document' => [
                "data" => base64_encode(file_get_contents($this->wrongfile)),
                "lastModifiedDate" => $datetime->format('c'),
                "name" => basename($this->wrongfile),
                "size" => filesize($this->wrongfile),
                "type" => mime_content_type($this->wrongfile)
            ]
        ])->assertResponseStatus(500);

    }

    public function testMissingFieldDocument()
    {
        $datetime = new \DateTime();
        $this->json('POST', 'api/documents?token='.$this->token, [
            'name' => 'test'
        ])->assertResponseStatus(400);

    }



    public function testGetDocuments()
    {
        $this->upload("test1");
        $this->upload("test2");

        $response = $this->get('api/documents?token='.$this->token)->response->content();

        $response = json_decode($response);
        $this->assertEquals(true, $response->count == 2);
        $this->assertEquals(true, isset($response->results));
    }


    public function upload($name) {

        $datetime = new \DateTime();

        return $this->json('POST', 'api/documents?token='.$this->token, [
            'name' => $name,
            'document' => [
                "data" => base64_encode(file_get_contents($this->file)),
                "lastModifiedDate" => $datetime->format('c'),
                "name" => basename($this->file),
                "size" => filesize($this->file),
                "type" => mime_content_type($this->file)
            ]
        ]);
    }


    public function tearDown()
    {
        $this->user->delete();
        $this->user->documents()->delete();
    }

}
